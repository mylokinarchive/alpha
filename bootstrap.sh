#!/usr/bin/env bash
apt-get update
apt-get install -y git python3
wget -O /tmp/virtualenv.py https://raw.github.com/pypa/virtualenv/master/virtualenv.py
python3 /tmp/virtualenv.py /opt/alpha
rm /tmp/virtualenv.py
/opt/alpha/bin/pip install alpha

mkdir /opt/alpharecipes/
chmod -R 755 /opt/alpha/
chmod -R 777 /opt/alpharecipes/

ln -s /opt/alpha/bin/alpha /usr/bin/alpha
/opt/alpha/bin/alpha install git://github.com/alpharecipes/git.git
/opt/alpha/bin/alpha install git://github.com/alpharecipes/profile.git
/opt/alpha/bin/alpha install git://github.com/alpharecipes/redis.git
