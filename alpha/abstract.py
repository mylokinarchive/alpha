from abc import *


class User(metaclass=ABCMeta):
    ''' User interface '''

    def __init__(self, username):
        self.username = username
        self.__home__ = None


    @abstractproperty
    def home(self):
        pass

    @abstractmethod
    def create(self, username):
        pass

    @abstractmethod
    def delete(self, username, home=False):
        pass

    @abstractmethod
    def exists(self, username):
        pass

    @abstractmethod
    def sudoer(self, username):
        pass

    @abstractmethod
    def set_password(self, username, password):
        pass


class Recipe(metaclass=ABCMeta):
    pass


class Package(Recipe, metaclass=ABCMeta):
    ''' Package interface '''
    dependencies = ()
    name = None

    def __init__(self, name=None):
        self.name = name

    @abstractmethod
    def install(self, dependencies=True):
        pass

    @abstractmethod
    def uninstall(self, dependencies=True):
        pass

    # @abstractmethod
    # def resolve_dependencies(cls):
    #     pass
