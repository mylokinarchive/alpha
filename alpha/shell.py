import os
import shlex
import subprocess
import sys
import itertools
import logging
from contextlib import contextmanager
from string import Template
from threading import Lock

from . import environ

logger = logging.getLogger('alpha')


class Shell(object):
    def __init__(self):
        self.__username__ = None
        self.__path__ = []
        self.__lock__ = Lock()

    def __call__(self, obj):
        obj.run = self.run
        return self

    @property
    def cwd(self):
        '''
        Current working directory
        '''
        if not self.__path__:
            return
        path = os.path.join(*self.__path__)
        return os.path.realpath(os.path.expanduser(path))

    def run(self, commands, raw=False):
        '''
        Run `command` in shell.
        `command` must be string. If it's multiline string then every string evalutes as separate command.
        Commands output could be accessed by settings parameter `raw` to True else will return True/False depends on status code.
        '''
        with self.__lock__:
            results = []
            for command in commands.split('\n'):
                command = command.strip()
                if not command:
                    continue
                command = command.format(**environ.env)
                if self.__username__:
                    command = 'sudo -u {username} {command}'.format(username=self.__username__, command=command)
                logger.info(command)
                results.append(self.execute(command, raw))
            return results[0] if len(results) == 1 else results

    def execute(self, command, raw):
        command = shlex.split(command)
        process = subprocess.Popen(command,
            universal_newlines=True,
            shell=False,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=0,
            cwd=self.cwd
        )
        out, err = process.communicate()
        logger.info(out or err)
        return out if raw else not bool(process.wait())

    @contextmanager
    def as_user(self, username):
        '''
        `sudo` wrapper. Avoid usage of ~ in paths.
        '''
        self.__username__ = username
        yield self
        self.__username__ = None

    @contextmanager
    def cd(self, path):
        '''
        Context manager to setup current working directory
        '''
        path = path.format(**environ.env)
        self.__path__.append(path)
        yield
        self.__path__.pop()

    def open(self, path, permissions=None):
        ''' creates empty file if doesn't exists '''
        path = path.format(**environ.env)
        try:
            raise RuntimeError()
        except RuntimeError:
            basepath = os.path.dirname(sys.exc_info()[-1].tb_frame.f_back.f_code.co_filename)

        if not path.startswith('/'):
            path = os.path.join(*itertools.chain(self.__path__, [path]))
        path = os.path.realpath(os.path.expanduser(path))

        if not os.path.exists(path):
            self.run('touch {}'.format(path))

        if permissions:
            self.run('chmod {} {}'.format(permissions, path))

        return File(path, basepath)


class File(object):
    def __init__(self, path, basepath):
        self.__path = path
        self.__basepath = basepath

    def write(self, template, context=None):
        if not template.startswith('/') and self.__basepath:
            template = os.path.join(self.__basepath, template)

        with open(template, 'r') as fp:
            content = fp.read()
        if context:
            content = Template(content).substitute(context)

        with open(self.__path, 'w+') as fp:
            fp.write(content)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def __repr__(self):
        return '<File:{} object at {}>'.format(self.__path, hex(id(self))[:-1])
