'''
    Environ object
    Provides shared state for recipes and uses as context for shell.* context managers

    Example:

    from alpha import environ
    environ.env['port'] = 80

    with cd('/{port}'):
        pass

'''
import copy
import collections
import functools


class Environ(collections.MutableMapping):
    def __init__(self):
        self.__storage = {}
        self.__state_stack = []

    def __setitem__(self, *args, **kwargs):
        return self.__storage.__setitem__(*args, **kwargs)

    def __getitem__(self, *args, **kwargs):
        return self.__storage.__getitem__(*args, **kwargs)

    def __delitem__(self, *args, **kwargs):
        return self.__storage.__delitem__(*args, **kwargs)

    def __iter__(self, *args, **kwargs):
        return self.__storage.__iter__(*args, **kwargs)

    def __len__(self, *args, **kwargs):
        return self.__storage.__len__(*args, **kwargs)

    def save(self):
        self.__state_stack.append(copy.deepcopy(self.__storage))

    def restore(self):
        self.__storage = self.__state_stack.pop()

env = Environ()

def clean(func):
    ''' Drop changes introduced to environ by decorated function or method '''
    env.save()
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        env.restore()
        return result
    return wrapper
