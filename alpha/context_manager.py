from functools import wraps
from . import __shell__

as_user = __shell__.as_user
cd = __shell__.cd


def executable(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        command = func(self, *args, **kwargs)
        return __shell__.run(command)
    return wrapper
