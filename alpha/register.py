import collections
import imp
import inspect
import itertools
import os

from . import abstract

PATH = ('/opt/alpharecipes', )


class Register(collections.Mapping):
    '''
    Actions register. Use it for access available actions from recipes.
    '''
    def __init__(self, paths):
        sources = itertools.chain(*map(self._lookup, paths))
        self.modules = self.__modules = list(self.__import(s) for s in sources)
        self.actions = self.__actions = {}
        [self.__actions.update(d) for d in (self.__lookup_function(m) for m in self.__modules)]
        [self.__actions.update(d) for d in (self.__lookup_methods(m) for m in self.__modules)]

    @staticmethod
    def _lookup(path):
        for dirpath, dirnames, filenames in os.walk(path):
            if '__init__.py' in filenames:
                del dirnames
                yield dirpath
                continue
            for f in (os.path.join(dirpath, f) for f in filenames if f.endswith('.py')):
                yield f

    @staticmethod
    def __lookup_function(module):
        functions = (f for name, f in inspect.getmembers(module) if inspect.isfunction(f))
        actions = (a for a in functions if hasattr(a, 'im_action'))
        return {'{}.{}'.format(module.__name__, a.__name__).lower(): a for a in actions}

    @staticmethod
    def __lookup_methods(module):
        instances = (c() for name, c in inspect.getmembers(module) if inspect.isclass(c) and issubclass(c, abstract.Recipe))
        methods = itertools.chain(*((m for name, m in inspect.getmembers(instance) if inspect.ismethod(m)) for instance in instances))
        actions = (m for m in methods if inspect.ismethod(m) and hasattr(m, 'im_action'))
        return {'{}.{}'.format(a.__self__.__class__.__name__, a.__name__).lower(): a for a in actions}

    @staticmethod
    def __import(source):
        name = os.path.basename(source)
        if source.endswith('.py'):
            name = name[:-3]
            return imp.load_source(name, source)
        else:
            module = imp.find_module(name, [os.path.dirname(source)])
            return imp.load_module(name, *module)

    def __getitem__(self, *args, **kwargs):
        return self.__actions.__getitem__(*args, **kwargs)

    def __iter__(self, *args, **kwargs):
        return self.__actions.__iter__(*args, **kwargs)

    def __len__(self, *args, **kwargs):
        return self.__actions.__len__(*args, **kwargs)

    def __repr__(self):
        return '<Register: {}... object at {}>'.format(','.join(self.__actions)[:10], hex(id(self))[:-1])

register = Register(PATH)
