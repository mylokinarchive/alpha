import functools

from . import __shell__

def which(executable):
    return __shell__.run('which {}'.format(executable), raw=True).strip()


def mkdir(dir_or_dirs):
    if not isinstance(dir_or_dirs, list):
        dir_or_dirs = [dir_or_dirs]
    command = '\n'.join(['mkdir -p {}'.format(dirictory) for dirictory in dir_or_dirs])
    return __shell__.run(command)


def action(func):
    func.im_action = True
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper
