from .. import abstract
from ..context_manager import executable


class User(abstract.User):
    @property
    def home(self):
        if not self.__home__:
            self.__home__ = '/home/{username}'.format(username=self.username)
        return self.__home__

    @executable
    def create(self):
        return 'useradd -d /home/{username} -m -s /bin/bash {username}'.format(username=self.username)

    @executable
    def delete(self, home=False):
        home = '-r' if home else ''
        return 'userdel {home} -f {username}'.format(username=self.username, home=home)

    @executable
    def exists(self):
        return 'getent passwd {username}'.format(username=self.username)

    @executable
    def sudoer(self):
        return 'adduser {username} sudo'.format(username=self.username)

    @executable
    def set_password(self, password):
        return 'usermod -p {password} {username}'.format(username=self.username, password=password)


class Package(abstract.Package):
    @executable
    def install(self):
        '''
        Installs package system-wide using apt-get

        Example:
        from alpha.platform import ubuntu
        ubuntu.Package('packagename').install()

        NOTE: last line performs in shell `apt-get install -y packagename`
        '''
        return 'apt-get install -y {name}'.format(name=self.name or self.__class__.name)

    @executable
    def uninstall(self):
        '''
        Uninstalls package system-wide using apt-get

        Example:
        from alpha.platform import ubuntu
        ubuntu.Package('packagename').uninstall()

        NOTE: last line performs in shell `apt-get remove -y packagename`
        '''
        return 'apt-get remove -y {name}'.format(name=self.name or self.__class__.name)

