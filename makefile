pass:
	$(info Alpha maintanance)
.PHONY: pass

clean: pass
	git clean -f
	rm -rf alpha.egg-info build dist alpha/__pycache__
.PHONY: clean

publish: pass
	git push origin master
	$(info Travis: https://travis-ci.org/alpharecipes/alpha)
.PHONY: push

pypi: pass
	$(info Publish alpha to PYPI)
	python setup.py sdist register upload
.PHONY: publish

deploy: pass publish pypi clean
	$(info Alpha deploy)
.PHONY: deploy

vm: pass
	cd vm && vagrant up && vagrant ssh
.PHONY: vm

vmdestroy: pass
	cd vm && vagrant destroy
.PHONY: destroy

vmrebuild: pass vmdestroy vm
	$(info refresh vm)
.PHONY: fresh

vminstall: pass
	apt-get update
	apt-get install -y git python3
	wget -O /tmp/virtualenv.py https://raw.github.com/pypa/virtualenv/master/virtualenv.py
	python3 /tmp/virtualenv.py /opt/alpha
	rm /tmp/virtualenv.py
	/opt/alpha/bin/python /home/vagrant/alpha/setup.py install

	chmod -R 755 /opt/alpha/
	mkdir /opt/alpharecipes/
	chmod -R 777 /opt/alpharecipes/

	ln -s /opt/alpha/bin/alpha /usr/bin/alpha
	/opt/alpha/bin/alpha install git://github.com/alpharecipes/git.git
	/opt/alpha/bin/alpha install git://github.com/alpharecipes/profile.git
	/opt/alpha/bin/alpha install git://github.com/alpharecipes/redis.git
.PHONY: vminstall
