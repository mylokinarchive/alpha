import os
import unittest

import alpha

import alpha.api
import alpha.context_manager
import alpha.environ
import alpha.platform.ubuntu
import alpha.register


class Api(unittest.TestCase):
    def test_entry_points(self):
        alpha.api.run
        alpha.api.fopen
        alpha.api.mkdir
        alpha.api.which
        alpha.context_manager.as_user
        alpha.context_manager.cd
        alpha.register


class ShellExecute(unittest.TestCase):
    def test_execute(self):
        execute = alpha.__shell__.execute
        self.assertTrue(execute('ls /', False))
        self.assertIn('etc', execute('ls /', True))


class Shell(unittest.TestCase):
    def setUp(self):
        self.run = alpha.api.run
        alpha.__shell__._execute = alpha.__shell__.execute
        alpha.__shell__.execute = lambda command, raw: command

    def tearDown(self):
        alpha.__shell__.execute = alpha.__shell__._execute
        del alpha.__shell__._execute

    def test_fn_run(self):
        self.assertEqual(self.run('cmd'), 'cmd')

    def test_fn_mkdir(self):
        mkdir = alpha.api.mkdir
        self.assertEqual(mkdir('/folder'), 'mkdir -p /folder')

    def test_fn_which(self):
        which = alpha.api.which
        self.assertEqual(which('cmd'), 'which cmd')

    def test_cm_as_user(self):
        as_user = alpha.context_manager.as_user

        with as_user('root'):
            self.assertEqual(self.run('cmd'), 'sudo -u root cmd')

        with as_user('root'):
            with as_user('test'):
                self.assertEqual(self.run('cmd'), 'sudo -u test cmd')

    def test_cm_cd(self):
        cd = alpha.context_manager.cd
        cwd = lambda: alpha.__shell__.cwd

        with cd('/tmp'):
            self.assertEqual(cwd(), '/tmp')

        with cd('/tmp'):
            with cd('subfolder'):
                self.assertEqual(cwd(), '/tmp/subfolder')

        with cd('~/'):
            self.assertEqual(cwd(), os.environ['HOME'])

        with cd('~/'):
            with cd('subfolder'):
                self.assertEqual(cwd(), os.path.join(os.environ['HOME'], 'subfolder'))

        with cd('~/'):
            with cd('..'):
                self.assertEqual(cwd(), '/home')

    def test_cm_environ(self):
        cd = alpha.context_manager.cd
        environ = alpha.environ
        cwd = lambda: alpha.__shell__.cwd

        environ.env['test'] = 'qwerty'
        with cd('/{test}'):
            self.assertEqual(cwd(), '/qwerty')

        self.assertIn('test', environ.env)
        del environ.env['test']
        self.assertNotIn('test', environ.env)

    def test_cm_environ_clean(self):
        cd = alpha.context_manager.cd
        environ = alpha.environ
        cwd = lambda: alpha.__shell__.cwd

        @environ.clean
        def test():
            environ.env['key'] = 'value'

        test()
        self.assertNotIn('key', environ.env)



class Templates(unittest.TestCase):
    def setUp(self):
        with open('/tmp/template', 'w+') as fp:
            fp.write('template ${var}')

        with open('/tmp/template_empty', 'w+') as fp:
            fp.write('template var')

    def tearDown(self):
        os.remove('/tmp/template')
        os.remove('/tmp/template_empty')
        if os.path.exists('/tmp/rendered'):
            os.remove('/tmp/rendered')

    def test_template_copy(self):
        fopen = alpha.api.fopen
        fopen('/tmp/rendered').write('/tmp/template_empty')
        with open('/tmp/rendered', 'r') as fp:
            content = fp.read()
            self.assertEqual(content, 'template var')

    def test_template_context(self):
        fopen = alpha.api.fopen
        fopen('/tmp/rendered').write('/tmp/template', {'var': 'value'})
        with open('/tmp/rendered', 'r') as fp:
            content = fp.read()
            self.assertEqual(content, 'template value')


class Recipe(unittest.TestCase):
    pass


class UbuntuPlatform(unittest.TestCase):
    pass



if __name__ == '__main__':
    unittest.main()
