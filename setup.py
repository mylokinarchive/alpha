try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import alpha

setup(
    name='alpha',
    version=alpha.__version__,
    description='System administration utils',
    author='Andrey Gubarev',
    author_email='andrey@base.so',
    scripts=['scripts/alpha'],
    packages=[
        'alpha',
        'alpha.platform'
    ],
    package_dir={
        'alpha': 'alpha',
        'alpha.platform': 'alpha/platform'
    }
)
